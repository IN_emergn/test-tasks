import { DalEntity } from "./dalEntity.type";
import { BlEntity } from "./blEntity.type";


export function createTree(dal: DalEntity[]): BlEntity[] {
  let res = [];
  let obj = <BlEntity>{};
  dal.map((item) => {
    let tmp = {
      id: item.id,
      text: item.text,
      children: []
    };
    if (item.parentId === null) {
      res.push(tmp);
    } else {
      if (!(item.parentId in obj)) {
        obj[item.parentId] = [];
      }
      obj[item.parentId].push(tmp);
    }
  });
  for (let i = 0; i < res.length; i++) {
    res[i] = this.addChildToParent(res[i], obj);
  }
  return res;
}

export function addChildToParent(node: BlEntity, childArray: any): BlEntity {
  let stackArr = [];
  stackArr.push(node);
  let bool;
  do {
    let nodeFromStack = stackArr.pop();
    let nodeWithChild = addChildToNode(nodeFromStack, childArray);
    if (nodeWithChild !== null) {
      nodeWithChild.children.map((obj) => {
        stackArr.push(obj);
      });
    }
    bool = stackArr.length !== 0;
  } while (bool);
  return node;



}

export function addChildToNode(node: BlEntity, childArray: any): BlEntity | null {
  if (node.id in childArray) {
    node.children = node.children.concat(childArray[node.id]);
    return node;
  } else {
    return null;
  }
}
