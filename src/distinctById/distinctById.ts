import { DistinctEntity } from "./entity.type";

/// <summary>
/// returns only objects with unique ids, dropping objects,
///  which ids are already in the output sequence
/// </summary>
/// <param name="entities">input collection</param>
/// <returns>distinct output collection</returns>
export function distinctById(entities: DistinctEntity[]): DistinctEntity[] {
  let obj = {};
  entities.map((item: DistinctEntity) => {
    if (!(item.id in obj)) {
       obj[item.id] = item;
    }
  });
  let res = [];
  Object.keys(obj).map((key) => {
    res.push(obj[key]);
  });
  return res;
}

