import { SingleEntity, PairEntity } from "./entities.type";

export function createPairs(left: SingleEntity[], right: SingleEntity[]): PairEntity[] {
  let leftAsObj = {};
  let res = [];
  left.map((item) => leftAsObj[item.id] = item);
  right.forEach((rightItem) => {
    if (rightItem.id in leftAsObj) {
      res.push( {
        "left": leftAsObj[rightItem.id],
        "right": rightItem
      });
    }
  });
  return res;

}
