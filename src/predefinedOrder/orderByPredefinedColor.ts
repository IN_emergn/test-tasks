import { ColoredEntity } from "./entity.type";
import { Color } from "./color.type";

/// <summary>
/// returns items from input collection in order, specified by "predefined" collection
/// </summary>
/// <param name="entities">input collection</param>
/// <param name="order">defines order</param>
/// <returns></returns>
export function orderByPredefinedColor(entities: ColoredEntity[], order: Color[]): ColoredEntity[] {
  let obj = {
  };
  entities.map((entity: ColoredEntity) => {
    if (!(entity.color in obj)) {
      obj[entity.color] = [];
    }
    obj[entity.color].push(entity);
  });
  let res: ColoredEntity[] = [];
  order.map((key) => {
    if (key in obj) {
      res = res.concat(obj[key]);
      delete obj[key];
    }
  });
  Object.keys(obj).map((key) => {
    res = res.concat(obj[key]);
  });
  return res;
}
