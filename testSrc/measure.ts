
// returns time in seconds that action has taken
export function measure(action: () => void): number {
  const start = new Date().getTime();
  action();
  const end = new Date().getTime();
  let res = (end - start);
  console.log(`Method performed in ${res} (msec)`);
  return res ; // end - start
}
