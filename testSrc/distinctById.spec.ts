import { expect } from "chai";
import { DistinctEntity } from "../src/distinctById/entity.type";
import { distinctById } from "../src/distinctById/distinctById";
import { testConstants } from "./testConstants";
import { measure } from "./measure";
import { areEquivalent, range } from "@vlr/array-tools";

describe("distinctById", function (): void {
  it("should return only unique entities", function (): void {
    // arrange
    const input: DistinctEntity[] = [
      { id: 1, name: "a" },
      { id: 4, name: "b" },
      { id: 1, name: "ab" },
      { id: 4, name: "b" },
      { id: 1, name: "ab" }
    ];

    const expected = [input[0], input[1]];

    // act
    const result = distinctById(input);

    // assert
    expect(areEquivalent(result, expected)).equals(true);
  });

  it("should pass performance constraint", function (): void {
    // arrange
    const input: DistinctEntity[] = range(testConstants.targetSize)
      .map(i => ({ id: i, name: "name" + i }));

    // act
    let result: DistinctEntity[];
    const time = measure(() => result = distinctById(input));

    // assert
    expect(time < testConstants.maxTime).equals(true);
    expect(result.length).equals(input.length);
  });
});
