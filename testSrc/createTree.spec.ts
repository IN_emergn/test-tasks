import { expect } from "chai";
import { createTree } from "../src/createTree/createTree";
import { BlEntity } from "../src/createTree/blEntity.type";
import { DalEntity } from "../src/createTree/dalEntity.type";
import { testConstants } from "./testConstants";
import { measure } from "./measure";

describe("createTree", function (): void {
  it("should return a tree TC1", function (): void {
    // arrange
    const input: DalEntity[] = [
      { id: 1, parentId: null, text: "a" },
      { id: 2, parentId: 1, text: "b" },
      { id: 3, parentId: 1, text: "c" },
      { id: 4, parentId: 2, text: "d" },
      { id: 5, parentId: 2, text: "bn" },
    ];

    const expected: BlEntity[] = [
      {
        id: 1,
        text: "a",
        children: [
          {
            id: 2, text: "b", children: [
              { id: 4, text: "d", children: [] },
              { id: 5, text: "bn", children: [] },
            ]
          },
          { id: 3, text: "c", children: [] }
        ]
      }
    ];

    // act
    const result = createTree(input);

    // assert
    compareTrees(expected, result);
  });

  it("should return a tree, TC2", function (): void {
    // arrange
    const input: DalEntity[] = [
      { id: 1, parentId: 3, text: "a" },
      { id: 2, parentId: 1, text: "b" },
      { id: 3, parentId: null, text: "c" },
      { id: 4, parentId: 2, text: "d" },
      { id: 5, parentId: null, text: "dd" },
    ];
    const expected: BlEntity[] = [
      {
        id: 3, text: "c", children: [
          {
            id: 1, text: "a", children: [
              {
                id: 2, text: "b", children: [
                  { id: 4, text: "d", children: [] }
                ]
              }
            ]
          }]
      },
      { "id": 5, "text": "dd", "children": [] }];

    // act
    const result = createTree(input);

    // assert
    compareTrees(expected, result);
  });

  it("should pass performance constraint", function (): void {
    // arrange
    const input: DalEntity[] = [{
      id: 1,
      parentId: null,
      text: "a"
    }];
    for (let i = 2; i < testConstants.targetSize; i++) {
      input.push({
        id: i,
        parentId: i - 1,
        text: "a"
      });
    }

    // act
    let result: BlEntity[];
    const time = measure(() => result = createTree(input));
    // assert
    expect(time < testConstants.maxTime).equals(true);
    expect(result.length).equals(1);
  });
});

function compareTrees(expected: BlEntity[], result: BlEntity[]): void {
  expect(result.length).equals(expected.length);
  expected.forEach(ex => compareNode(ex, result.find(r => r.id === ex.id)));
}

function compareNode(ex: BlEntity, result: BlEntity): void {
  expect(result == null).equals(false);
  expect(result.text).equals(ex.text);
  compareTrees(ex.children, result.children);
}


