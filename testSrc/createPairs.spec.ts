import { expect } from "chai";
import { createPairs } from "../src/createPairs/createPairs";
import { PairEntity, SingleEntity } from "../src/createPairs/entities.type";
import { testConstants } from "./testConstants";
import { measure } from "./measure";
import { range } from "@vlr/array-tools";

describe("createPairs", function (): void {
  it("should return all pairs", function (): void {
    // arrange
    const left: SingleEntity[] = [{ id: 1, text: "a" }, { id: 3, text: "b" }, { id: 4, text: "z" }];
    const right: SingleEntity[] = [{ id: 2, text: "c" }, { id: 1, text: "d" }, { id: 4, text: "y" }];

    const expected = [
      { left: left[0], right: right[1] },
      { left: left[2], right: right[2] }
    ];

    // act
    const result = createPairs(left, right);

    // assert
    expect(result.length).equals(expected.length);
    expected.forEach(ex => {
      expect(null != result.find(r => r.left === ex.left && r.right === ex.right)).equals(true);
    });
  });

  it("should pass performance constraint", function (): void {
    // arrange
    const input: SingleEntity[] = range(testConstants.targetSize)
      .map(i => ({ id: i, text: "some text" }));

    // act
    let result: PairEntity[];
    const time = measure(() => result = createPairs(input, input));
    // assert
    expect(time < testConstants.maxTime).equals(true);
    expect(result.length).equals(input.length);
  });

});

